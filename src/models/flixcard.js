const mongoose = require('mongoose')
const validator = require('validator')

const flixcardSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true,
		trim: true,
		validate(value) {
			if (value.length < 5)
				throw new Error('title is too short')
		}
	},
	description: {
		type: String,
		required: true,
		trim: true,
		validate(value) {
			if (value.length < 10)
				throw new Error('description is too short')
		}
	},
	tags: {
		type: Array,
		required: true,
		validate(value) {
			if (value.length < 1)
				throw new Error('there must be at least one tag')
		}
	},
	status: {
		learn: {
			done: {
				type: Number,
				default: 0,
				validate(value) {
					if (typeof value !== 'number')
						throw new Error('something went wrong, please report this error')
				}
			},
			state: {
				type: Number,
				default: null,
				validate(value) {
					if (value && typeof value !== 'number')
						throw new Error('something went wrong, please report this error')
				}
			}
		},
		study: {
			done: {
				type: Number,
				default: 0,
				validate(value) {
					if (typeof value !== 'number')
						throw new Error('something went wrong, please report this error')
				}
			},
			state: {
				type: Number,
				default: null,
				validate(value) {
					if (value && typeof value !== 'number')
						throw new Error('something went wrong, please report this error')
				}
			}
		},
		challenge: {
			done: {
				type: Number,
				default: 0,
				validate(value) {
					if (typeof value !== 'number')
						throw new Error('something went wrong, please report this error')
				}
			},
			state: {
				type: Number,
				default: null,
				validate(value) {
					if (value && typeof value !== 'number')
						throw new Error('something went wrong, please report this error')
				}
			}
		}
	},
	cards: [{
		question: {
			type: String,
			required: true
		},
		answer: {
			type: String,
			required: true
		}
	}],
	owner: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: 'User'
	}
}, {
	timestamps: true
})

const Flixcard = mongoose.model('Flixcard', flixcardSchema)

module.exports = Flixcard