const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const Flixcard = require('./flixcard')

const userSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		trim: true
	},
	email: {
		type: String,
		unique: true,
		required: true,
		trim: true,
		lowercase: true,
		validate(value) {
			if(!validator.isEmail(value)) {
				throw new Error('Email is invalid')
			}
		}
	},
	password: {
		type: String,
		required: true,
		validate(value) {
			if (value.length < 6 || !(/[a-zA-Z]/g.test(value)) || !(/\d/g.test(value)))
				throw new Error('Password not safe')
		}
	},
	birthday: {
		type: String,
		default: '',
		trim: true,
		validate(value) {
			if (value !== '' && !validator.isDate(value)) {
				throw new Error('Date is invalid')
			}
		}
	},
	tokens: [{
		token: {
			type: String,
			required: true
		}
	}]
}, {
	timestamps: true
})

// user-flixcard relation
userSchema.virtual('flixcards', {
	ref: 'Flixcard',
	localField: '_id',
	foreignField: 'owner'
})

// removes sensible data from response on login/signup
userSchema.methods.toJSON = function() {

	const user = this
	const userObject = user.toObject()

	delete userObject.password
	delete userObject.tokens

	return userObject


}

// generats an authentication token and sends it back
userSchema.methods.generateAuthToken = async function() {

	const user = this
	const token = jwt.sign({ _id: user._id.toString() }, 'flixapp', { expiresIn: '7 days' })

	user.tokens = user.tokens.concat({ token })
	await user.save()

	return token


}

// find user, compare hasedpassword and typed in password
userSchema.statics.findByCredentials = async (email, password) => {

	const user = await User.findOne({ email })

	if (!user) {
		throw new Error('Unable to login')
	}

	const isMatch = await bcrypt.compare(password, user.password)

	if (!isMatch) {
		console.log('no token')
		throw new Error('Unable to login')
	}

	return user


}

// hash plain text password before saving
userSchema.pre('save', async function(next) {

	const user = this

	if (user.isModified('password'))
		user.password = await bcrypt.hash(user.password, 8)

	next()


})

// delete user flixcards when user is removed
userSchema.pre('remove', async function(next) {

	const user = this

	await Flixcard.deleteMany({ owner: user._id })

	next()


})

const User = mongoose.model('User', userSchema)

module.exports = User