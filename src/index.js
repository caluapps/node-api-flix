const express = require('express')
require('./db/mongoose')
const userRouter = require('./routers/user')
const flixcardRouter = require('./routers/flixcard')

const app = express()
const port = process.env.PORT || 3000


/* Maintenance Mode
	app.use((req, res, next) => {
		res.status(503).send('Maintenance Mode. Service currently not available. Check back soon!')		
	}) */

app.use(express.json())
app.use(userRouter)
app.use(flixcardRouter)

app.listen(port, () => {
	console.log(`Server is up on port ${port}`)
})


