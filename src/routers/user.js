const express = require('express')
const User = require('../models/user')
const auth = require('../middleware/auth')
const router = new express.Router()


// create user
router.post('/users', async (req, res) => {

	const user = new User(req.body)

	try {
		await user.save()

		const token = await user.generateAuthToken()

		res.status(201).send({ user, token })

	} catch (e) {
		console.log("error post '/users'", user)
		console.log('error:', e)

		res.status(400).send(e)
	}
})

// login user
router.post('/users/login', async (req,res) => {

	try {
		const user = await User.findByCredentials(req.body.email, req.body.password)
		const token = await user.generateAuthToken()

		res.send({ user, token })

	} catch(e) {
		console.log("error post '/users/login'")
		console.log('error:', e)

		res.status(400).send()
	}
})

// logout user
router.post('/users/logout', auth, async (req, res) => {

	try {
		req.user.tokens = req.user.tokens.filter(token => req.token !== token.token)

		await req.user.save()

		res.send()

	} catch(e) {
		console.log("error post '/users/logout'")
		console.log('error:', error)

		res.status(500).send(e)
	}
})

// logout all session
router.post('/users/logoutAll', auth, async (req, res) => {

	try {
		req.user.tokens = []

		await req.user.save()

		res.send()

	} catch(e) {
		console.log("error path post '/users/logoutAll'")
		console.log('error:', error)

		res.status(500).send(e)
	}
})

// fetch user profile - me
router.get('/users/me', auth, async (req, res) => {

	res.send(req.user)
})

// update user - me
router.patch('/users/me', auth, async (req, res) => {

	const updates = Object.keys(req.body)
	const allowedUpdates = ['name', 'email', 'password', 'birthday']
	const isInvalidOperation = !updates.every(key => allowedUpdates.includes(key))

	if (isInvalidOperation)
		return res.status(400).send({ error: 'Invalid updates!' })

	try {
		updates.forEach(update => req.user[update] = req.body[update])

		await req.user.save()

		res.send(req.user)

	} catch(e) {
		console.log("error patch '/users/:id'", req.body)
		console.log('error:', e)

		res.status(400).send(e)
	}
})

// delete user - me
router.delete('/users/me', auth, async (req, res) => {

	try {
		await req.user.remove()

		res.send(req.user)

	} catch(e) {
		console.log("error delete '/users/:id'", req.params.id)
		console.log('error:', e)

		res.status(400).send(e)
	}
})


// admin - fetch users
router.get('/users', auth, async (req, res) => {

	try {
		const users = await User.find({})
		res.send(users)

	} catch(e) {
		console.log("error get '/users'")
		console.log('error:', e)

		res.status(500).send(e)
	}
})

// admin - fetch user
router.get('/users/:id', auth, async (req, res) => {

	try {
		const user = await User.findById(req.params.id)
		if (!user)
			return res.status(404).send()

		res.send(user)

	} catch(e) {
		console.log("error get '/user/:id'", id)
		console.log('error:', e)

		res.status(500).send(e)
	}
})

// admin - update user
router.patch('/users/:id', async (req, res) => {

	const updates = Object.keys(req.body)
	const allowedUpdates = ['name', 'email', 'password']
	const isInvalidOperation = !updates.every(key => allowedUpdates.includes(key))

	if (isInvalidOperation)
		return res.status(400).send({ error: 'Invalid updates!' })

	try {
		const user = await User.findById(req.params.id)

		if (!user)
			return res.status(404).send()

		updates.forEach(update => user[update] = req.body[update])

		await user.save()

		res.send(user)

	} catch(e) {
		console.log("error patch '/users/:id'", req.body)
		console.log('error:', e)

		res.status(400).send(e)
	}
})

// admin - delete user
router.delete('/users/:id', auth, async (req, res) => {

	try {
		const deletedUser = await User.findByIdAndRemove(req.params.id)

		if (!deletedUser)
			return res.status(404).send()

		res.send(deletedUser)

	} catch(e) {
		console.log("error delete '/users/:id'", req.params.id)
		console.log('error:', e)

		res.status(400).send(e)
	}
})


module.exports = router
