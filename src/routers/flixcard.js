const express = require('express')
const Flixcard = require('../models/flixcard')
const auth = require('../middleware/auth')
const router = new express.Router()

// create flixcard
router.post('/flixcards', auth, async (req, res) => {

	const flixcard = new Flixcard({
		...req.body,
		owner: req.user._id
	})

	try {
		await flixcard.save()

		res.status(201).send(flixcard)

	} catch(e) {
		console.log("error post '/flixcards'", flixcard)
		console.log('error:', e)

		res.status(400).send(e)
	}
})

// fetch own flixcards
	// GET /flixcards ? new=true
	// GET /flixcards ? limit=10 & skip=20
	// GET /flixcards ? sortBy=createdAt_asc dsc
router.get('/flixcards', auth, async (req, res) => {

	const match = { }
	const sort = { }

	if (req.query.notdone && req.query.notdone === 'true') {

		match['$or'] = [
			{
				'status.learn.done': { ['$eq']: 0 }
			}, {
				'status.study.done': { ['$eq']: 0 }
			}, {
				'status.challenge.done': {	['$eq']: 0 }
			}
		]
	}

	if (req.query.sortBy) {

		const [ sortBy, dir ] = req.query.sortBy.split`:`

		sort[sortBy] = dir == 'asc' ? 1 : -1
	}

	try {
		/* without populate
			const flixcards = await Flixcard.find({ owner: req.user._id })

			res.send(flixcards) */

		await req.user.populate({
			path: 'flixcards',
			match,
			options: {
				limit: parseInt(req.query.limit),
				skip: parseInt(req.query.skip),
				sort
			}
		}).execPopulate()

		res.send(req.user.flixcards)

	} catch(e) {
		console.log("get flixcards '/flixcards'")
		console.log('error:', e)

		res.status(500).send(e)
	}
})

// fetch own flixcard
router.get('/flixcards/:id', auth, async (req, res) => {

	const _id = req.params.id

	try {
		const flixcard = await Flixcard.findOne({ _id, owner: req.user._id })

		if (!flixcard)
			return res.status(404).send()

		res.send(flixcard)

	} catch(e) {
		console.log("get '/flixcard/:id'", req.params.id)
		console.log('error:', e)

		res.status(500).send(e)
	}
})

// update own flixcard
router.patch('/flixcards/:id', auth, async (req, res) => {

	const updates = Object.keys(req.body)
	const allowedUpdates = ['title', 'description', 'tags', 'status', 'cards']
	const isInvalidOperation = !updates.every(key => allowedUpdates.includes(key))

	if (isInvalidOperation)
		return res.status(400).send({ error: 'Invalid updates!' })

	try {
		const flixcard = await Flixcard.findOne({ _id: req.params.id, owner: req.user._id })

		if (!flixcard)
			return res.status(404).send()

		updates.forEach(update => flixcard[update] = req.body[update])

		await flixcard.save()

		res.send(flixcard)

	} catch(e) {
		console.log("error patch '/flixcards/:id'", req.body)
		console.log('error:', e)

		return res.status(400).send(e)
	}
})

// delete flixcard
router.delete('/flixcards/:id', auth, async (req, res) => {

	try {
		const deletedFlixcard = await Flixcard.findOneAndDelete({ _id: req.params.id, owner: req.user._id })

		if (!deletedFlixcard)
			return res.status(404).send()

		res.send(deletedFlixcard)

	} catch(e) {
		console.log("error delete '/flixcard/:id'")
		console.log('error:', e)

		res.status(400).send()
	}
})

module.exports = router
